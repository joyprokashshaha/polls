from django.urls import include, path

from quickstart.views import *

urlpatterns = [
    path('quickstart/users/', UserViewSet, name='UserViewSet'),
    path('quickstart/groups/', GroupViewSet, name='GroupViewSet'),

    path('quickstart/users/', UserViewSet),
    path('quickstart/groups/', GroupViewSet),
]