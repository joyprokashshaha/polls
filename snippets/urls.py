from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

urlpatterns = [
    # function based view
    path('snippets/', snippet_list, name='snippet_list'),
    path('snippets/<int:pk>/', snippet_detail, name='snippet_detail'),

    # class based view
    path('snippet/', SnippetList.as_view(), name='SnippetList'),
    path('snippet/<int:pk>/', SnippetDetail.as_view(), name='SnippetDetail'),

    # mixins based class view
    path('mixins/snippet/', SnippetMixinsList.as_view(), name='SnippetMixinsList'),
    path('mixins/snippet/<int:pk>/', SnippetMixinsDetail.as_view(), name='SnippetMixinsDetail'),

    # generic class based view
    path('generic/snippet/', SnippetListView.as_view(), name='SnippetListView'),
    path('generic/snippet/<int:pk>/', SnippetDetailView.as_view(), name='SnippetDetailView'),
]

urlpatterns = format_suffix_patterns(urlpatterns)