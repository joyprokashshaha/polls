from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import polls_list, polls_detail, index, detail, results, vote, IndexView, DetailView, ResultsView
from .apiviews import PollList, PollDetail, ChoiceList, CreateVote, UserCreate, LoginView


from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Polls API')

app_name = 'polls'

'''Pure Django'''
# urlpatterns = [
#     path("polls/", polls_list, name="polls_list"),
#     path("polls/<int:pk>/", polls_detail, name="polls_detail")
# ]

'''DRF APIView and Generic Views'''
urlpatterns = [
    # views
    # path('', index, name='index'),
    # # ex: /5/
    # path('<int:poll_id>/', detail, name='detail'),
    # # ex: /5/results/
    # path('<int:poll_id>/results/', results, name='results'),

    path('', IndexView.as_view(), name='index'),
    path('<int:pk>/', DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', ResultsView.as_view(), name='results'),
    # ex: /5/vote/
    path('<int:poll_id>/vote/', vote, name='vote'),

    # apiviews
    path("polls/", PollList.as_view(), name="polls_list"),
    path("polls/<int:pk>/", PollDetail.as_view(), name="polls_detail"),
    # path("choices/", ChoiceList.as_view(), name="choice_list"),  # for create choice and show list
    path("polls/<int:pk>/choices/", ChoiceList.as_view(), name="choice_list"),  # for showing choice list by individual poll
    path("polls/<int:pk>/choices/<int:choice_pk>/vote/", CreateVote.as_view(), name="create_vote"),

    path("users/", UserCreate.as_view(), name="user_create"),
    path("login/", LoginView.as_view(), name="login"),

    path(r'swagger-docs/', schema_view),
]


'''Viewsets and Router'''
# router = DefaultRouter()
# router.register('polls', PollViewSet, basename='polls')
#
# urlpatterns = [
#     path("polls/", PollList.as_view(), name="polls_list"),
#     path("polls/<int:pk>/", PollDetail.as_view(), name="polls_detail"),
#
#     path("polls/<int:pk>/choices/", ChoiceList.as_view(), name="choice_list"),  # for showing choice list by individual poll
#     path("polls/<int:pk>/choices/<int:choice_pk>/vote/", CreateVote.as_view(), name="create_vote"),
#
#     path("users/", UserCreate.as_view(), name="user_create"),
#     path("login/", LoginView.as_view(), name="login"),
# ]
#
# urlpatterns += router.urls
