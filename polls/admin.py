from django.contrib import admin

from .models import Poll, Choice, Vote

# Register your models here.


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['question']}),
        ('Create information', {'fields': ['created_by']}),
    ]
    list_display = ('question', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['question']
    inlines = [ChoiceInline]


admin.site.register(Poll, PollAdmin)
admin.site.register(Choice)
admin.site.register(Vote)
